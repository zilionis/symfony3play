import Vue from 'vue';
import App from './app.vue';
import BootstrapVue from 'bootstrap-vue'
import VeeValidate from 'vee-validate';
import axios from 'axios';
import VueRouter from 'vue-router'
import createPersistedState from 'vuex-persistedstate'

const config = {
    errorBagName: 'errors', // change if property conflicts.
    fieldsBagName: 'inputs ', //Default is fields
    delay: 0,
    locale: 'en',
    dictionary: null,
    strict: true,
    enableAutoClasses: false,
    classNames: {
        touched: 'touched', // the control has been blurred
        untouched: 'untouched', // the control hasn't been blurred
        valid: 'valid', // model is valid
        invalid: 'invalid', // model is invalid
        pristine: 'pristine', // control has not been interacted with
        dirty: 'dirty' // control has been interacted with
    },
    events: 'input|blur',
    inject: true
};

Vue.use(VeeValidate, config);
Vue.use(BootstrapVue);
Vue.use(VueRouter);

let myAxios = axios.create({
    headers: {'Content-Type': 'application/json'}
});

import HomePage from './pages/homepage.vue';
import Login from './pages/login.vue';
import Users from './pages/users.vue';

const router = new VueRouter({
    routes: [
        { path: '/', component: HomePage },
        { path: '/login', component: Login },
        { path: '/users', component: Users },
    ]
});

import Vuex from 'vuex'

Vue.use(Vuex);

const store = new Vuex.Store({
    plugins: [createPersistedState()],
    state: {
        username: '',
        accessToken: '',
    },
    mutations: {
        login (state, {username, accessToken}) {
            state.username = username;
            state.accessToken = accessToken;
        },
        cleanup (state) {
            state.username = '';
            state.accessToken = '';
        }
    }
});

Vue.prototype.$http = myAxios;

let app = new Vue({
    store,
    router,
    components: {
        App
    }
}).$mount('#app');

window.apsas = app;