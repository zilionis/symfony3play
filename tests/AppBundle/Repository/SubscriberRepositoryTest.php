<?php

namespace Test\AppBundle\Repository;

use AppBundle\Entity\Subscriber;
use AppBundle\Repository\SubscriberRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class SubscriberRepositoryTest extends KernelTestCase
{
    /**
     * @var SubscriberRepository
     */
    private $repository;

    public function setUp()
    {
        parent::setUp();
        self::bootKernel();
        $this->repository = static::$kernel->getContainer()->get('subscribe.repository');
        $this->repository->removeAllData();
    }

    public function testShouldParseFileCorrectly() {
        $this->repository->setFilePath(__DIR__ . '/demo.json', true);
        $this->assertEquals(2, $this->repository->subscribersCount());

        $subscriber1 = $this->repository->findByEmail('nice@example.com');
        $this->assertInstanceOf(Subscriber::class, $subscriber1);
        $this->assertEquals('Nice Name', $subscriber1->getName());
        $this->assertEquals('5c68bbb0-ad95-4cbf-8712-b73c663979a5', $subscriber1->getUuid());
        $this->assertEquals('2017-11-07 13:01:59', $subscriber1->getCreatedAt()->format("Y-m-d H:i:s"));

        $subscriber2 = $this->repository->findByUid('275b9ec3-a92b-486e-8426-0145b5605d65');
        $this->assertInstanceOf(Subscriber::class, $subscriber2);
        $this->assertEquals('Some Name', $subscriber2->getName());
        $this->assertEquals('some@example.com', $subscriber2->getEmail());

    }

    public function testShouldAllowSaveSubscriber() {

        $givenEmail = 'email1@example.com';
        $givenName = 'Name 1';
        $this->repository->save($this->givenSubscriber($givenEmail, $givenName));
        $this->assertEquals(1, $this->repository->subscribersCount());

        $maybeSubscriber = $this->repository->findByEmail($givenEmail);
        $this->assertInstanceOf(Subscriber::class, $maybeSubscriber);
        $this->assertEquals($givenName, $maybeSubscriber->getName());
    }

    public function testShouldAllowSaveMultiSubscriber() {

        $this->repository->save($this->givenSubscriber('email1@example.com'));
        $this->repository->save($this->givenSubscriber('email2@example.com'));
        $this->repository->save($this->givenSubscriber('email3@example.com'));

        $this->assertEquals(3, $this->repository->subscribersCount());
    }

    public function testShouldAllowRemoveSubscriber() {

        $email = 'email2delete@example.com';
        $subscriber = $this->repository->save($this->givenSubscriber($email));

        $this->assertEquals(1, $this->repository->subscribersCount());
        $this->repository->remove($subscriber->getUuid());
        $this->assertEquals(0, $this->repository->subscribersCount());

    }

    public function testShouldUpdateSubscriberWithSameEmail()
    {
        $givenEmail = 'email1@example.com';
        $givenNewName = 'Name 2';
        $mockDate = new \DateTime("2017-01-01 00:00:00");

        $this->repository->save($this->givenSubscriber($givenEmail, 'Name 1', null, $mockDate));

        $updatedSubscriber = $this->givenSubscriber($givenEmail, $givenNewName);
        $this->assertNotEquals($mockDate, $updatedSubscriber->getCreatedAt());
        $this->repository->save($updatedSubscriber);
        $this->assertEquals(1, $this->repository->subscribersCount());

        $maybeSubscriber = $this->repository->findByEmail($givenEmail);
        $this->assertInstanceOf(Subscriber::class, $maybeSubscriber);
        $this->assertEquals($givenNewName, $maybeSubscriber->getName());
        $this->assertEquals($mockDate, $maybeSubscriber->getCreatedAt());
    }

    protected function givenSubscriber(
        string $email,
        string $name = 'Some Name',
        ?array $categories = null,
        ?\DateTime $createTime = null,
        ?string $uuid = null): Subscriber {

        if ($createTime === null) {
            $createTime = new \DateTime();
        }

        if ($categories === null) {
            $categories = ['p'];
        }

        $data = new Subscriber();
        $data
            ->setUuid($uuid)
            ->setEmail($email)
            ->setName($name)
            ->setCategories($categories)
            ->setCreatedAt($createTime);

        return $data;
    }
}