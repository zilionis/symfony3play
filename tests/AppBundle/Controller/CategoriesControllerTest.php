<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Repository\SubscriberRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Tests\ApiHelper\ApiTestCase;

class CategoriesControllerTest extends ApiTestCase
{

    public function testFetchCategoriesList() {
        /** @var JsonResponse $jsonResponse */
        $this->amAnonymousUser();

        $jsonResponse = $this->getJson('/api/newsletter/categories')->getResponse();
        $this->assertEquals(Response::HTTP_OK, $jsonResponse->getStatusCode());
        $data = json_decode($jsonResponse->getContent(), true);

        $this->assertEquals(3, count($data), 'Categories should be with 3 items');

        $this->assertHasValue($data, 'id', 'a1');
        $this->assertHasValue($data, 'id', 'b2');
        $this->assertHasValue($data, 'id', 'c3');
    }
}
