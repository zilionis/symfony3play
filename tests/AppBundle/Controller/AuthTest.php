<?php

namespace Test\AppBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Tests\ApiHelper\ApiTestCase;

class AuthTest extends ApiTestCase
{
    public function testAuthFailure() {

        $response = $this->makeAuth('incorrectUser', 'incorrectPassword');

        $this->assertEquals(401, $response->getStatusCode());
    }

    public function testAuthClientSuccess() {
        /** @var JsonResponse $response */

        $this->accessToken = null;
        $this->amAuthorizedUser();

        $this->assertNotNull($this->accessToken);
    }

    public function testAccessSecuredUriWithAnonymous() {
        $this->amAnonymousUser();

        $jsonResponse = $this->getJson('/api/me')->getResponse();

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $jsonResponse->getStatusCode());
    }

    public function testAccessSecuredUriWithAuthorized() {
        $this->amAuthorizedUser();

        $jsonResponse = $this->getJson('/api/me')->getResponse();

        $this->assertEquals(Response::HTTP_OK, $jsonResponse->getStatusCode());
        $this->assertEquals('{"username":"test_user"}', $jsonResponse->getContent());
    }


}