<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Repository\SubscriberRepository;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Tests\ApiHelper\ApiTestCase;

class SubscriberControllerTest extends ApiTestCase
{
    /**
     * @var SubscriberRepository
     */
    private $repository;

    public function setUp()
    {
        self::bootKernel();

        $this->repository = static::$kernel->getContainer()->get('subscribe.repository');
        $this->repository->removeAllData();
    }

    public function testRequestShouldFailForNotJsonContentType()
    {
        $this->amAnonymousUser();
        $client = static::createClient();
        $server = array();
        $server['HTTP_AUTHORIZATION'] = sprintf('Bearer %s', $this->accessToken);

        $client->request('POST', $this->uriCreate, [], [], $server, '{}');

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_UNSUPPORTED_MEDIA_TYPE, $response->getStatusCode());

    }

    public function validationProvider()
    {
        return [
            'not_blank' => [
                $this->givenRegistration(['email' => null]), 'email', 'This value should not be blank.'
            ],
            'invalid_email' => [
                $this->givenRegistration(['email' => 'invalidEmail']), 'email', 'This value is not a valid email address.'
            ],
            'name_is_required' => [
                $this->givenRegistration(['name' => '']), 'name', 'This value should not be blank.'
            ],
            'name_to_short' => [
                $this->givenRegistration(['name' => 'a']), 'name', 'This value is too short. It should have 3 characters or more.'
            ],
            'name_to_long' => [
                $this->givenRegistration(['name' => str_repeat('t', 101)]), 'name', 'This value is too long. It should have 100 characters or less.'
            ],
            'categories_is_empty' => [
                $this->givenRegistration(['categories' => []]), 'categories', 'You must select at least 1 choice.'
            ],
            'categories_with_invalid_key' => [
                $this->givenRegistration(['categories' => ['notExist']]), 'categories', 'One or more of the given values is invalid.'
            ],
        ];
    }

    /**
     * @dataProvider validationProvider
     * @param $data
     * @param $field
     * @param $expectedErrorMessage
     * @internal param $emailContent
     */
    public function testCreateValidations(array $data, string $field, string $expectedErrorMessage)
    {
        $this->amAnonymousUser();
        /** @var JsonResponse $response */
        $response = $this->createSubscriber($data)->getResponse();

        $this->assertHasError($response, $field, $expectedErrorMessage);
    }
    public function testShouldNotAllowToChangeEmailToExistingOne()
    {
        $this->amAuthorizedUser();

        $email1 = 'email1@example.com';
        $email2 = 'email2@example.com';

        $this->assertEquals(0, count($this->getAllSubscribers()));
        $this->createSubscriber($this->givenRegistration(['email' => $email1]));

        $subscriberData = $this->givenRegistration(['email' => $email2]);
        $this->createSubscriber($subscriberData);

        $this->assertEquals(2, count($this->getAllSubscribers()));

        $data = $this->getSubscriberByEmail($email2);
        $this->assertEquals($email2, $data['email']);

        $subscriberData['email'] = $email1;
        $subscriberData['uuid'] = $data['uuid'];

        /** @var JsonResponse $response */
        $response = $this->updateSubscriber($subscriberData)->getResponse();

        $this->assertHasError($response, 'email', 'Email exists');
    }

    public function testUpdateWithInvalidUuid()
    {
        $this->amAuthorizedUser();
        $this->createSubscriber($this->givenRegistration(['email' => 'example@example.com']));

        $forUpdate = $this->givenRegistration(['email' => 'another@email.com', 'uuid' => 'uuid-not-exist']);

        /** @var JsonResponse $response */
        $response = $this->updateSubscriber($forUpdate)->getResponse();
        $this->assertHasError($response, 'uuid', 'This is not a valid UUID.');
    }


    protected function givenRegistration(array $config = []): array
    {
        $correct = [
            'email' => 'valid@example.com',
            'name' => 'Chuck Norris',
            'categories' => ['a1']
        ];

        return array_merge($correct, $config);
    }

    protected function createSubscriber($data): Client
    {
        return $this->postJson($this->uriCreate, json_encode($data));
    }

    protected function updateSubscriber($data): Client
    {
        return $this->postJson($this->uriUpdate, json_encode($data));
    }

    protected function getAllSubscribers($params = []): array
    {
        $response = $this->getJson($this->uriGetSubscribers, $params)->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        return json_decode($response->getContent(), true);
    }

    protected function getSubscriberByEmail($string)
    {
        $data = $this->getAllSubscribers(['email' => $string]);

        $this->assertEquals(1, count($data));

        return $data[0];
    }
}
