<?php

namespace Tests\ApiHelper;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

abstract class ApiTestCase extends WebTestCase
{

    protected $uriGetSubscribers = '/api/newsletter/subscribers';
    protected $uriCreate = '/api/newsletter/subscribe';
    protected $uriUpdate = '/api/newsletter/subscribers/update';

    protected $accessToken;
    protected $accessTokenUsername;

    public function setUp()
    {
        parent::setUp();

        $this->amAnonymousUser();
    }

    protected function amAnonymousUser()
    {
        $this->accessToken = null;
        $this->accessTokenUsername = null;
    }


    protected function amAuthorizedUser()
    {
        $username = 'test_user';
        $password = 'test_password';

        if ($this->accessToken && $this->accessTokenUsername == $username) {
            return;
        }

        $token = $this->getAuthenticatedToken($username, $password);
        if (null === $token) {
            throw new \LogicException('Unable to create an authenticated client from a null JWT token');
        }

        $this->accessTokenUsername = $username;
        $this->accessToken = $token;
    }


    private function getAuthenticatedToken($userName, $password)
    {
        $response = $this->makeAuth($userName, $password);
        $this->assertEquals(200, $response->getStatusCode());

        $responseBody = json_decode($response->getContent(), true);

        return $responseBody['token'];
    }


    protected function makeAuth($userName, $password): Response
    {
        $client = static::createClient();
        $client->request('POST', '/login_check', [
            '_username' => $userName,
            '_password' => $password,
        ]);

        return $client->getResponse();
    }

    /**
     * @param string $uri
     * @param string $content
     * @return Client
     */
    protected function postJson(string $uri, string $content): Client
    {
        return $this->makeRequest('POST', $uri, [],[], [], $content);
    }

    protected function putJson(string $uri, string $content): Client
    {
        return $this->makeRequest('PUT', $uri, [],[], [], $content);
    }

    protected function getJson(string $uri, array $params = []): Client
    {
        return $this->makeRequest('GET', $uri, $params);
    }

    private function makeRequest($method, $uri, array $params = [], array $files = [], $server = [], $content = null): Client
    {
        $client = static::createClient();
        $server['CONTENT_TYPE'] = 'application/json';
        if ($this->accessToken)
        {
            $server['HTTP_AUTHORIZATION'] = sprintf('Bearer %s', $this->accessToken);
        }

        $client->request($method, $uri, $params, $files, $server, $content);

        return $client;
    }

    protected function assertHasError(Response $response, string $field, string $errorMessage) {

        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
        $this->assertResponseIsJson($response);

        $responseData = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('errors', $responseData);
        $this->assertArrayHasKey($field, $responseData['errors']);
        $this->assertEquals($errorMessage, $responseData['errors'][$field]);
    }

    protected function assertHasValue(array $data, $key, $value)
    {
        $success = false;
        foreach ($data as $row) {
            if (isset($row[$key]) && $row[$key] === $value) {
                $success = true;
                break;
            }
        }

        $this->assertTrue($success, "Has not value '$value' for key '$key'");

    }

    protected function assertResponseIsJson($response)
    {
        $this->assertTrue($response->headers->contains('Content-Type', 'application/json'), 'Invalid JSON response');
    }
}

