<?php

namespace AppBundle\Service;

use ReflectionClass;
use ReflectionProperty;
use Symfony\Component\HttpFoundation\RequestStack;

class JsonRequest
{
    protected $request;
    protected $requestParser;

    public function __construct(RequestStack $requestStack,
                                RequestParser $requestParser
    )
    {
        $this->request = $requestStack->getCurrentRequest();
        $this->requestParser = $requestParser;

        $this->init();
    }

    private function init()
    {
        $this->requestParser->validateContentType($this->request->headers->get('content_type'));
        $validPayload = $this->requestParser->validatePayload(
            $this->request->getContent(),
            get_class($this),
            'json'
        );

        $this->applyParams($validPayload);
    }

    protected function applyParams($payload)
    {
        $reflectionClass = new ReflectionClass($payload);
        $properties = $reflectionClass->getProperties(ReflectionProperty::IS_PUBLIC);
        foreach ($properties as $property) {
            $this->{$property->name} = $payload->{$property->name};
        }
    }
}