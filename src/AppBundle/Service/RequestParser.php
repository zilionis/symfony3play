<?php

namespace AppBundle\Service;

use AppBundle\Exception\InvalidPayloadException;
use Doctrine\Common\Inflector\Inflector;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class RequestParser
{
    private $validContentTypes = ['json' => 'application/json'];
    private $serializer;
    private $validator;
    private $inflector;

    public function __construct(
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        Inflector $inflector
    )
    {
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->inflector = $inflector;
    }

    public function validatePayload($payload, $model, $format)
    {
        $payload = $this->serializer->deserialize($payload, $model, $format);
        $errors = $this->validator->validate($payload);

        if (count($errors)) {
            throw $this->createFailureResponse($errors);
        }
        return $payload;
    }

    public function validateContentType($contentType)
    {
        if (!in_array($contentType, $this->validContentTypes)) {
            throw $this->createFailureResponse(
                ['content_type' => sprintf('Invalid content type [%s].', $contentType)],
                415
            );
        }

        return array_search($contentType, $this->validContentTypes);
    }

    protected function createFailureResponse($content, $status = 400)
    {

        $errorList = null;

        if ($content instanceof ConstraintViolationList) {

            foreach ($content as $error) {

                $error = $this->getErrorFromValidation($error);

                $errorList[$error['key']] = $error['value'];

            }

        } else {
            $errorList = $content;
        }

        return new InvalidPayloadException(serialize(['errors' => $errorList]), $status);
    }

    private function getErrorFromValidation(ConstraintViolation $constraintViolation)
    {
        $properties = $this->inflector->tableize($constraintViolation->getPropertyPath());
        return [
            'key' => $properties,
            'value' => $constraintViolation->getMessage()
        ];
    }
}