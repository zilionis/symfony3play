<?php

namespace AppBundle\EventListener;

use AppBundle\Exception\InvalidPayloadException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\Security\Core\Exception\InsufficientAuthenticationException;

class ExceptionListener
{
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if ($exception instanceof InvalidPayloadException) {
            $event->setResponse(new JsonResponse(unserialize($exception->getMessage()), $exception->getCode()));
        }
        elseif ($exception instanceof InsufficientAuthenticationException) {
            $event->setResponse(new JsonResponse("Auth needed", 403));
        }

        return;
    }
}