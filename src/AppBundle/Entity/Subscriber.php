<?php

namespace AppBundle\Entity;

class Subscriber
{
    protected $uuid;

    protected $name;

    protected $email;

    protected $categories = [];

    protected $createdAt;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param array $categories
     * @return $this
     */
    public function setCategories(array $categories = [])
    {
        $this->categories = ($categories === null) ? [] : $categories;

        return $this;
    }

    /**
     * @param mixed $createdAt
     * @return Subscriber
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     * @return $this
     */
    public function setUuid(?string $uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function toArray() {
        return [
            'uuid' => $this->getUuid(),
            'name' => $this->getName(),
            'email' => $this->getEmail(),
            'categories' => $this->getCategories(),
            'createdAt' => $this->getCreatedAt()->format("Y-m-d H:i:s"),
        ];
    }

    static public function setFromArray($row)
    {
        $s = new self();
        $s->setUuid($row['uuid'])
            ->setEmail($row['email'])
            ->setName($row['name'])
            ->setCategories($row['categories'])
            ->setCreatedAt(new \DateTime($row['createdAt']));

        return $s;
    }
}
