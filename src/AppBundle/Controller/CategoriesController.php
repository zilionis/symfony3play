<?php

namespace AppBundle\Controller;

use AppBundle\Repository\CategoryRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\User\UserInterface;

class CategoriesController extends Controller
{
    /**
     * @Route("/api/newsletter/categories")
     * @Method("GET")
     * @param CategoryRepository $repository
     * @return Response
     */
    public function getNewsletterListAction(CategoryRepository $repository): Response
    {
        return new JsonResponse(
            $repository->getAll()
        );
    }
}