<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class UserController
{
    /**
     * @Route("/api/me")
     * @Method("GET")
     * @param UserInterface $user
     * @return JsonResponse
     */
    public function getMeAction(UserInterface $user): JsonResponse
    {
        return new JsonResponse(
            [
                'username' => $user->getUsername()
            ]
        );
    }
}