<?php

namespace AppBundle\Controller;

use AppBundle\Exception\InvalidPayloadException;
use AppBundle\Repository\SubscriberRepository;
use AppBundle\Request\SubscribeCreateRequest;
use AppBundle\Request\SubscribeDeleteRequest;
use AppBundle\Request\SubscribeUpdateRequest;
use JMS\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SubscriberController extends Controller
{

    /**
     * @Route("/api/newsletter/subscribe")
     * @Method("POST")
     * @param SubscribeCreateRequest $payload
     * @param SubscriberRepository $repository
     * @return Response
     */
    public function createAction(SubscribeCreateRequest $payload, SubscriberRepository $repository): Response
    {
        $repository->save($payload->getSubscriber());

        return new JsonResponse([
            'success' => true,
        ]);
    }

    /**
     * @Route("api/newsletter/subscribers/delete")
     * @param SubscribeDeleteRequest $payload
     * @param SubscriberRepository $repository
     * @return JsonResponse
     */
    public function deleteSubscriber(SubscribeDeleteRequest $payload, SubscriberRepository $repository)
    {
        $repository->remove($payload->uuid);

        return new JsonResponse([
            'success' => true
        ]);
    }

    /**
     * @Route("/api/newsletter/subscribers")
     * @param SubscriberRepository $repository
     * @param Request $request
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function getSubscribers(SubscriberRepository $repository, Request $request, SerializerInterface $serializer)
    {
        $maybeEmail = $request->get('email');
        $filters = [];
        if ($maybeEmail) {
            $filters['email'] = $maybeEmail;
        }
        $subscribers = $repository->getAll($filters)->getValues();

        return new JsonResponse($serializer->serialize($subscribers, 'json'), 200, [], true);
    }

    /**
     * @Route("/api/newsletter/subscribers/update")
     * @Method("POST")
     * @param SubscribeUpdateRequest $payload
     * @param SubscriberRepository $repository
     * @return Response
     */
    public function updateAction(SubscribeUpdateRequest $payload, SubscriberRepository $repository)
    {

        $repository->save($payload->getSubscriber());

        return new JsonResponse([
            'success' => true,
        ]);
    }
}