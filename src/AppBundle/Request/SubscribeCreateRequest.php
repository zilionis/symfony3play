<?php

namespace AppBundle\Request;

use AppBundle\Entity\Subscriber;
use AppBundle\Repository\CategoryRepository;
use AppBundle\Service\JsonRequest;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

class SubscribeCreateRequest extends JsonRequest
{
    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=3, max=100)
     * @Serializer\Type("string")
     * @var string
     */
    public $name;

    /**
     * @Assert\NotBlank()
     * @Assert\Email()
     * @Serializer\Type("string")
     * @var string
     */
    public $email;

    /**
     * @Assert\Choice(
     *   strict=true,
     *   multiple=true,
     *   min=1,
     *   callback = "getCategoriesKeys",
     *   message = "Invalid categories"
     * )
     * @Serializer\Type("array")
     * @var array
     */
    public $categories;

    public function getCategoriesKeys()
    {
        $repo = new CategoryRepository();
        return $repo->getKeys();
    }

    public function getSubscriber(): Subscriber
    {
        $subscriber = new Subscriber();
        $subscriber
            ->setEmail($this->email)
            ->setName($this->name)
            ->setCategories($this->getCategories())
            ->setCreatedAt(new \DateTime());

        return $subscriber;
    }

    public function getCategories()
    {
        if (!$this->categories) {
            return [];
        }

        return $this->categories;
    }
}