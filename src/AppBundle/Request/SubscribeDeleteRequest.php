<?php

namespace AppBundle\Request;

use AppBundle\Service\JsonRequest;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

class SubscribeDeleteRequest extends JsonRequest
{
    /**
     * @Assert\Uuid()
     * @Serializer\Type("string")
     * @var string
     */
    public $uuid;
}