<?php

namespace AppBundle\Request;

use AppBundle\Entity\Subscriber;
use AppBundle\Service\JsonRequest;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

class SubscribeUpdateRequest extends SubscribeCreateRequest
{
    /**
     * @Assert\Uuid()
     * @Serializer\Type("string")
     * @var string
     */
    public $uuid;

    public function getSubscriber(): Subscriber {
        $subscriber = new Subscriber();
        $subscriber
            ->setUuid($this->uuid)
            ->setEmail($this->email)
            ->setName($this->name)
            ->setCategories($this->getCategories())
            ->setCreatedAt(new \DateTime());

        return $subscriber;
    }
}