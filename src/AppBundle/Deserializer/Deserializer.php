<?php
namespace AppBundle\Deserializer;

use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\SerializerInterface;

class Deserializer
{
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function deserializeArray($jsonString, string $entityClass)
    {
        $collection = new ArrayCollection();
        $rows = json_decode($jsonString, true);

        foreach ($rows as $key => $row) {
            $collection->set($key, $this->convertToEntity($row, $entityClass));
        }

        return $collection;
    }

    /**
     * @param $config
     * @param $entityClass
     * @return mixed
     */
    protected function convertToEntity(array $config, string $entityClass)
    {
        return $entityClass::setFromArray($config);
    }
}