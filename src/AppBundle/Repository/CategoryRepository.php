<?php

namespace AppBundle\Repository;

class CategoryRepository
{
    protected static $categories = [
        'a1' => 'Category 1',
        'b2' => 'Category 2',
        'c3' => 'Category 3',
    ];

    public function getKeys()
    {
        return array_keys(self::$categories);
    }

    public function getAll()
    {
        $list = [];
        foreach (self::$categories as $categoryId => $categoryName) {
            $list[] = [
                'id' => $categoryId,
                'name' => $categoryName,
            ];
        }

        return $list;
    }

    public function keyExist($key)
    {
        return isset($this->categories[$key]);
    }
}