<?php

namespace AppBundle\Repository;

use AppBundle\Deserializer\Deserializer;
use AppBundle\Entity\Subscriber;
use AppBundle\Exception\InvalidPayloadException;
use Doctrine\Common\Collections\ArrayCollection;
use Ramsey\Uuid\Uuid;

class SubscriberRepository
{
    protected $filePath;

    protected $contents;

    /** @var ArrayCollection */
    protected $subscribers;

    protected $isReadOnly = false;

    protected $deserializer;

    public function __construct($path, Deserializer $deserializer)
    {
        $this->filePath = $path;
        $this->deserializer = $deserializer;
        $this->readSubscribers();
    }

    protected function readSubscribers()
    {
        $this->subscribers = new ArrayCollection();
        if (file_exists($this->filePath)) {
            $this->subscribers = $this->deserializer->deserializeArray(
                file_get_contents($this->filePath),
                Subscriber::class
            );
        }
    }

    public function getAll(array $filter = [])
    {
        $subscribers = $this->subscribers;
        if (!empty($filter['email'])) {
            $email = $filter['email'];
            $subscribers = $this->subscribers->filter(function(Subscriber $s) use ($email) {
               return strtolower($s->getEmail()) === strtolower($email);
            });
        }

        return $subscribers;
    }

    /**
     * @param string $path
     * @param bool $isReadOnly
     */
    public function setFilePath(string $path, $isReadOnly = false)
    {
        $this->filePath = $path;
        $this->isReadOnly = $isReadOnly;
        $this->readSubscribers();
    }

    public function subscribersCount()
    {
        return $this->subscribers->count();
    }

    public function removeAllData()
    {
        $this->subscribers = new ArrayCollection();
        $this->saveDatabase();
    }

    protected function saveDatabase()
    {
        if ($this->isReadOnly) {
            return;
        }

        $convertedData = $this->subscribers->map(function (Subscriber $obj) {
            return $obj->toArray();
        })->toArray();

        file_put_contents($this->filePath, json_encode($convertedData, JSON_PRETTY_PRINT));
    }

    public function save(Subscriber $newSubscriber): Subscriber
    {
        if ($newSubscriber->getUuid() === null) {
            $subscriber = $this->saveNew($newSubscriber);
        } else {
            $subscriber = $this->saveUpdate($newSubscriber);
        }

        $this->subscribers->set($subscriber->getUuid(), $subscriber);
        $this->saveDatabase();

        return $subscriber;
    }

    public function findByEmail(string $email): ?Subscriber
    {
        return $this->findOne(function (Subscriber $data) use ($email) {
            return $data->getEmail() === $email;
        });
    }

    protected function findOne($closure): ?Subscriber
    {
        $data = $this->subscribers->filter($closure);

        if ($data->count() === 1) {
            return $data->get($data->getKeys()[0]);
        }

        return null;
    }

    protected function updateSubscriberData(
        Subscriber $oldSubscriber,
        Subscriber $newSubscriber,
        bool $updateCategories = true)
    {
        $oldSubscriber
            ->setEmail($newSubscriber->getEmail())
            ->setName($newSubscriber->getName());

        if ($updateCategories) {
            $oldSubscriber->setCategories($newSubscriber->getCategories());
        }

        return $oldSubscriber;
    }

    public function findByUid(string $uuid): ?Subscriber
    {
        return $this->findOne(function (Subscriber $data) use ($uuid) {
            return $data->getUuid() === $uuid;
        });
    }

    public function remove($uuid)
    {
        $this->subscribers->remove($uuid);
        $this->saveDatabase();
    }

    /**
     * @param Subscriber $newSubscriber
     * @return $this|Subscriber|null
     */
    protected function saveNew(Subscriber $newSubscriber)
    {
        $subscriber = $this->findByEmail($newSubscriber->getEmail());

        if ($subscriber) {
            $subscriber = $this->updateSubscriberData($subscriber, $newSubscriber, true);
        } else {
            $subscriber = $newSubscriber->setUuid(Uuid::UUID4()->toString());
        }
        return $subscriber;
    }

    /**
     * @param Subscriber $newSubscriber
     * @return Subscriber|null
     */
    protected function saveUpdate(Subscriber $newSubscriber)
    {
        $subscriber = $this->findByUid($newSubscriber->getUuid());
        $maybeEmail = $this->findByEmail($newSubscriber->getEmail());

        if (!$subscriber) {
            throw new InvalidPayloadException(serialize(['errors' => ['email' => ['uuid' => 'Uuid not found']]]), 400);
        }
        else if ($maybeEmail && $maybeEmail->getUuid() !== $subscriber->getUuid()) {
            throw new InvalidPayloadException(serialize(['errors' => ['email' => 'Email exists']]), 400);
        }

        return $this->updateSubscriberData($subscriber, $newSubscriber, false);
    }
}
